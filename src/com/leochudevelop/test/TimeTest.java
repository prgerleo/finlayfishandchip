package com.leochudevelop.test;

import com.leochudevelop.Solution;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by leochu on 7/8/2016.
 */
public class TimeTest {
    private final String DEFAULT_TIME_ONE = "12:00:00";

    private final int DEFAULT_TOTAL_MILLISECOND_ONE = 43200;

    private final int DEFAULT_HOUR_ONE = 12;
    private final int DEFAULT_MIN_ONE = 0;
    private final int DEFAULT_SEC_ONE = 0;

    private final String DEFAULT_TIME_TWO = "12:01:10";

    private final int DEFAULT_TOTAL_MILLISECOND_TWO = 43200 + 60 + 10;

    private final int DEFAULT_HOUR_TWO = 12;
    private final int DEFAULT_MIN_TWO = 1;
    private final int DEFAULT_SEC_TWO = 10;

    private Solution.Time sampleTimeOne;
    private Solution.Time sampleTimeTwo;

    private List<Solution.Time> expectedArray;

    @Before
    public void setUp() throws Exception {
        sampleTimeOne = new Solution.Time(DEFAULT_TIME_ONE);
        sampleTimeTwo = new Solution.Time(DEFAULT_TIME_TWO);

        expectedArray = new ArrayList<>();
        expectedArray.add(sampleTimeOne);
        expectedArray.add(sampleTimeTwo);
    }

    @Test
    public void testSampleOne() throws Exception {
        assertEquals(DEFAULT_HOUR_ONE , sampleTimeOne.getHour());
        assertEquals(DEFAULT_MIN_ONE , sampleTimeOne.getMinute());
        assertEquals(DEFAULT_SEC_ONE , sampleTimeOne.getSecond());

        assertEquals(DEFAULT_TOTAL_MILLISECOND_ONE, sampleTimeOne.getTotalInSeconds());
    }

    @Test
    public void testSampleTwo() throws Exception {
        assertEquals(DEFAULT_HOUR_TWO , sampleTimeTwo.getHour());
        assertEquals(DEFAULT_MIN_TWO , sampleTimeTwo.getMinute());
        assertEquals(DEFAULT_SEC_TWO , sampleTimeTwo.getSecond());

        assertEquals(DEFAULT_TOTAL_MILLISECOND_TWO, sampleTimeTwo.getTotalInSeconds());
    }

    @Test
    public void testCompareTo() {
        assertTrue(sampleTimeTwo.compareTo(sampleTimeOne) > 0);
    }

    @Test
    public void testCompare() {
        assertTrue(sampleTimeOne.compare(sampleTimeTwo, sampleTimeOne) > 0);
    }

    @Test
    public void testCollection() {
        List<Solution.Time> sampleArray = new ArrayList<>();
        sampleArray.add(sampleTimeTwo);
        sampleArray.add(sampleTimeOne);

        Collections.sort(sampleArray);

        for (int i = 0; i < sampleArray.size(); i++) {
            assertEquals(expectedArray.get(i), sampleArray.get(i));
        }
    }
}