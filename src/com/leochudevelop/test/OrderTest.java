package com.leochudevelop.test;

import com.leochudevelop.Solution;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by leochu on 8/9/16.
 */
public class OrderTest {
    private List<Solution.Order> orderList;

    private List<Integer> expectedAnswerList;

    @Before
    public void setUp() throws Exception {
        orderList = new ArrayList<>();

        try(BufferedReader br = new BufferedReader(new FileReader("test/order_test_cases/input001.txt"))) {
            String line = br.readLine();
            while (line != null) {
                orderList.add(new Solution.Order(line));

                line = br.readLine();
            }
            br.close();
        }

        expectedAnswerList = new ArrayList<>();

        try(BufferedReader br2 = new BufferedReader(new FileReader("test/order_test_cases/output001.txt"))) {
            String line = br2.readLine();
            while (line != null) {
                expectedAnswerList.add(Integer.valueOf(line));

                line = br2.readLine();
            }
            br2.close();
        }
    }

    @After
    public void tearDown() throws Exception {
        orderList.clear();
        orderList = null;
    }

    @Test
    public void testOrder() throws Exception {
        int i = 0;
        for (Solution.Order order: orderList) {
            assertEquals(expectedAnswerList.get(i++).intValue(), order.getServeTimeInSecond());
        }
    }
}