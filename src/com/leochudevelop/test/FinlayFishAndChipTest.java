package com.leochudevelop.test;

import com.leochudevelop.Solution;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by leochu on 8/9/16.
 */
@RunWith(Parameterized.class)
public class FinlayFishAndChipTest {
    private static final String FOLDER_NAME = "test/solution_test_cases/";

    private static final Object[] TEST_FILES_NUM = new Object[] {0,1,2,3,4,5,6,7,8,9};

    private StringBuilder getIntputFromInputFile(File file) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();

        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = br.readLine();
            while (line != null) {
                stringBuilder.append(line).append('\n');
                line = br.readLine();
            }
            br.close();
        }
        return stringBuilder;
    }

    private StringBuilder getOutputFromOutputFile(File file) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();

        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = br.readLine();
            while (line != null) {
                stringBuilder.append(line).append('\n');
                line = br.readLine();
            }
            br.close();
        }
        return stringBuilder;
    }

    private void testCase(File inputFile, File outputFile) throws Exception{
        StringBuilder input = getIntputFromInputFile(inputFile);
        StringBuilder expectedOutput = getOutputFromOutputFile(outputFile);

        Solution.FinlayFishAndChip shop = new Solution.FinlayFishAndChip();

        String[] orders = input.toString().split("\n");
        for (String order:orders) {
            if (!order.isEmpty())
                shop.placeOrder(order);
        }
        shop.executeOrders();
        assertEquals(expectedOutput.toString(), shop.getKitchenLog().toString());

        System.out.println("Test (" + inputFile.toString() + ")" + " success");
    }

    @Parameters
    public static Collection<Object> data() {
        return Arrays.asList(TEST_FILES_NUM);
    }

    @Parameter
    public int mCaseNumber;

    @Test
    public void test() throws Exception {
        String inputFileName = String.format(FOLDER_NAME+"input%03d.txt", mCaseNumber);
        String outputFileName  = String.format(FOLDER_NAME+"output%03d.txt", mCaseNumber);

        File inputFile = new File (inputFileName);
        File outputFile = new File (outputFileName);

        System.out.println("Start test " + inputFileName + ".");
        testCase(inputFile, outputFile);
    }
}