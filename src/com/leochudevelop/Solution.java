package com.leochudevelop;

import java.util.*;

public class Solution {
    // helper inner classes
    public static final class Time implements Comparable<Time>, Comparator<Time> {
        private static final String DEFAULT_TIME = "00:00:00";

        private static final String COLON_SEPARATOR = ":";

        private static final short ZERO = 0;

        private static final short MAX_HOURS = 24;
        private static final short MAX_MIN_AND_SEC = 60;
        private static final short HOUR_IN_SECOND = MAX_MIN_AND_SEC * MAX_MIN_AND_SEC;

        private short mHour;
        private short mMinute;
        private short mSecond;
        private int mTotalInSeconds;

        public Time() {
            setTime(DEFAULT_TIME);
        }

        public Time(String time) {
            setTime(time);
        }

        public Time(Time time) {
            setTime(time);
        }

        public short getHour() {
            return mHour;
        }

        public short getMinute() {
            return mMinute;
        }

        public short getSecond() {
            return mSecond;
        }

        public int getTotalInSeconds() {
            return mTotalInSeconds;
        }

        /**
         * set the time with string format "00:00:00".
         *
         * @param time string format "00:00:00"
         */
        public void setTime(String time) {
            String[] parts = time.split(COLON_SEPARATOR);

            mHour = Short.valueOf(parts[0]);
            mMinute = Short.valueOf(parts[1]);
            mSecond = Short.valueOf(parts[2]);

            mTotalInSeconds = mHour * HOUR_IN_SECOND + mMinute * MAX_MIN_AND_SEC + mSecond;

            checkIllegalCondition();
        }

        /**
         * set the time with another Time object.
         *
         * @param time {@Code Time}
         */
        public void setTime(Time time) {
            mTotalInSeconds = time.getTotalInSeconds();

            mHour = time.getHour();
            mMinute = time.getMinute();
            mSecond = time.getSecond();

            checkIllegalCondition();
        }

        /**
         * add time in second.
         *
         * @param second add millisecond to the current Time object
         */
        public void add(int second) {
            mTotalInSeconds += second;

            mHour = (short)(mTotalInSeconds / HOUR_IN_SECOND);
            mMinute = (short)((mTotalInSeconds % HOUR_IN_SECOND) / MAX_MIN_AND_SEC);
            mSecond = (short)(mTotalInSeconds % MAX_MIN_AND_SEC);
        }

        /**
         * get the time difference in second.
         *
         * @param time the target time
         * @return the time difference between two time in second
         */
        public int diff(Time time) {
            return mTotalInSeconds - time.getTotalInSeconds();
        }

        private void checkIllegalCondition(){
            if (mHour < Time.ZERO || mHour > MAX_HOURS) {
                throw new IllegalArgumentException("Invalid hour");
            } else if (mMinute < Time.ZERO || mMinute > MAX_MIN_AND_SEC){
                throw new IllegalArgumentException("Invalid minute");
            } else if (mSecond < Time.ZERO || mSecond > MAX_MIN_AND_SEC){
                throw new IllegalArgumentException("Invalid second");
            }
        }

        @Override
        public String toString() {
            return String.format("%02d:%02d:%02d", mHour, mMinute, mSecond);
        }

        @Override
        public int compareTo(Time otherTime) {
            return new Integer(mTotalInSeconds).compareTo(otherTime.getTotalInSeconds());
        }

        @Override
        public int compare(Time timeOne, Time timeTwo) {
            return Integer.compare(timeOne.getTotalInSeconds(), timeTwo.getTotalInSeconds());
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof Time &&
                    getTotalInSeconds() == ((Time) obj).getTotalInSeconds();
        }
    }

    private static final class LogHelper {
        private static StringBuilder getAcceptLog(Order order) {
            return new StringBuilder("at ").append(order.getOrderTime().toString()).append(", ")
                    .append(order.toString()).append(" Accepted");
        }

        private static StringBuilder getServeLog(Order order, Time serveTime) {
            return new StringBuilder("at ").append(serveTime.toString()).append(", ")
                    .append("Serve ").append(order.toString());
        }

        private static StringBuilder getRejectLog(Order order) {
            return new StringBuilder("at ").append(order.getOrderTime().toString()).append(", ")
                    .append(order.toString()).append(" Rejected");
        }

        private static StringBuilder getBeginCookingLog(Time cookTime,
                                                        int cookAmount, String foodName) {
            return new StringBuilder("at ").append(cookTime.toString()).append(", ")
                    .append("Begin Cooking ").append(cookAmount).append(" ").append(foodName);
        }
    }

    // FinlayFishAndChip inner classes
    public static final class FinlayFishAndChip {
        private final Kitchen mKitchen;
        private StringBuilder mKitchenLog;

        public FinlayFishAndChip() {
            mKitchen = new Kitchen();
            mKitchenLog = new StringBuilder();
        }

        public void executeOrders() {
            mKitchenLog = new StringBuilder();
            while (mKitchen.hasPendingOrder()) {
                mKitchen.executeOrder();
                mKitchenLog.append(mKitchen.getOrderLog());
            }
            mKitchen.closeOrders();
        }

        public StringBuilder getKitchenLog() {
            return mKitchenLog;
        }

        public void placeOrder(String orderInString) {
            Order order = new Order(orderInString);
            mKitchen.receiveOrder(order);
        }
    }

    private static final class Kitchen {
        private static final byte WORKERS_COUNT = 2;
        private static final byte FISH_FRYER = 0;
        private static final byte CHIP_FRYER = 1;

        private final List<IFryer> mFryers;

        private final LinkedList<Order> mPendingOrderList;

        private final TreeSet<IFryer> mPendingCookingFryers;

        private final HashMap<String, Integer> mFryersCount;

        private StringBuilder mOrderLog;

        private Time mReadyToOrderTime;

        public Kitchen() {
            mOrderLog = new StringBuilder();

            mPendingOrderList = new LinkedList<>();

            mFryersCount = new HashMap<>();

            mPendingCookingFryers = new TreeSet<>();

            mFryers = new ArrayList<>(WORKERS_COUNT);
            addFryer(FISH_FRYER, new FishFryer());
            addFryer(CHIP_FRYER, new ChipFryer());
        }

        private void addFryer(int index, IFryer fryer){
            mFryers.add(index, fryer);

            if (mFryersCount.containsKey(fryer.getTag())){
                int count = mFryersCount.get(fryer.getTag());
                mFryersCount.put(fryer.getTag(), count+1);
            } else {
                mFryersCount.put(fryer.getTag(), 1);
            }
        }

        public void closeOrders() {
            mOrderLog = null;

            mReadyToOrderTime = null;

            mPendingOrderList.clear();

            mPendingCookingFryers.clear();

            mFryersCount.clear();
        }

        public void executeOrder() {
            // initiate a new order log
            mOrderLog = new StringBuilder();

            // get the next order if there is any in the queue.
            Order order = mPendingOrderList.poll();

            // the difference between the order time and the fryer ready time.
            int diff = order.getOrderTime().diff(mReadyToOrderTime);

            // check if the ready to cook time is earlier than the order time.
            if (diff > Time.ZERO) {
                // update the start cooking time to avoid pre-cooked food.
                mReadyToOrderTime.setTime(order.getOrderTime());
            }

            // check is the order meet the good quality requirement.
            // accept the order if it is met, else reject it.
            if (order.isOrderAccepted(mReadyToOrderTime)) {
                // initiate all fryers cooking state
                for (IFryer fryer: mFryers) {
                    fryer.receiveOrder(order);
                    fryer.setStartCookingTime(mReadyToOrderTime);
                    fryer.addDelay(order);

                    if (fryer.hasOrder()){
                        mPendingCookingFryers.add(fryer);
                    }
                }

                // update serve time.
                mReadyToOrderTime.add(order.getServeTimeInSecond());

                // print Order #0 Accepted.
                mOrderLog.append(LogHelper.getAcceptLog(order)).append('\n');

                // print Begin Cooking ...
                while (!mPendingCookingFryers.isEmpty()) {
                    // print the time in order.
                    IFryer minIFryer = mPendingCookingFryers.pollFirst();
                    mOrderLog.append(minIFryer.beginCooking());
                    if (minIFryer.hasOrder()){
                        mPendingCookingFryers.add(minIFryer);
                    }
                }

                // print Serve Order #0.
                mOrderLog.append(LogHelper.getServeLog(order, mReadyToOrderTime)).append('\n');

            } else {
                // print Order #0 Rejected.
                mOrderLog.append(LogHelper.getRejectLog(order)).append('\n');
            }
        }

        public StringBuilder getOrderLog() {
            return mOrderLog;
        }

        public Order getLastOrder() {
            return mPendingOrderList.isEmpty() ? null : mPendingOrderList.getLast();
        }

        public boolean hasPendingOrder() {
            return !mPendingOrderList.isEmpty();
        }

        public void receiveOrder(Order order) {
            if (getLastOrder() != null) {
                if (order.mOrderNumber < getLastOrder().mOrderNumber) {
                    throw new IllegalArgumentException("Number not in order");
                } else if (order.getOrderTime().compareTo(getLastOrder().getOrderTime()) < Time.ZERO) {
                    throw new IllegalArgumentException("Time not in order");
                }
            }

            // get the first order time.
            if (mReadyToOrderTime == null){
                mReadyToOrderTime = new Time(order.getOrderTime());
            }

            // add order to the pending queue.
            mPendingOrderList.addLast(order);
        }
    }

    private enum Food {
        BatteredCod("Cod", 80),
        BatteredHaddock("Haddock", 90),
        Chips("Chips", 120);

        private final int mCookingTime;
        private final String mName;

        /**
         * initiate a food enum object which contains the food name and the cook time.
         *
         * @param name the food name
         * @param cookingTime the cook time
         */
        Food(String name, int cookingTime) {
            mName = name;
            mCookingTime = cookingTime;
        }

        /**
         * get the Food object by passing the food name.
         *
         * If there is no input food type, return {@Code null}.
         * @param name the food name
         * @return {@Code Food}
         */
        public static Food fromString(String name) {
            for (Food food: values()) {
                if (food.mName.equals(name)){
                    return food;
                }
            }
            return null;
        }

        /**
         * get cooking time.
         *
         * @return cooking time in second
         */
        public int getCookingTime() {
            return mCookingTime;
        }

        /**
         * get food name.
         *
         * @return food name
         */
        public String getName() {
            return mName;
        }
    }

    /**
     * This class is to store the food type and the food count from {@Code Order}.
     * It is able to calculate and return the server food time for the specific food type,
     * and also able to check is the first cooked food cold for specific food type.
     */
    private static abstract class FoodInOrder implements Comparable<FoodInOrder>, Comparator<FoodInOrder>{
        private static final byte EMPTY = 0;

        private final HashMap<Food, Integer> mFoods;

        public FoodInOrder(HashMap<Food, Integer> foods) {
            mFoods = new HashMap<>(foods);
        }

        public boolean containsFoodType(Food food) {
            return mFoods.containsKey(food);
        }

        public int getFoodCount(Food food) {
            return containsFoodType(food) ? mFoods.get(food) : EMPTY;
        }

        public abstract Integer getServeTime();
        public abstract boolean isFirstCookedFoodCold();

        @Override
        public int compareTo(FoodInOrder otherFoodOrder) {
            return getServeTime().compareTo(otherFoodOrder.getServeTime());
        }

        @Override
        public int compare(FoodInOrder foodOrderOne, FoodInOrder foodOrderTwo) {
            return foodOrderOne.getServeTime().compareTo(foodOrderTwo.getServeTime());
        }
    }

    private static final class FishFoodOrder extends FoodInOrder {
        public static final String TAG = FishFoodOrder.class.getSimpleName();

        public FishFoodOrder(HashMap<Food, Integer> foods) {
            super(foods);

            for (Food food: foods.keySet()) {
                if (food != Food.BatteredCod && food != Food.BatteredHaddock) {
                    throw new IllegalArgumentException("Not fish!");
                }
            }
        }

        @Override
        public Integer getServeTime() {
            int codCount = getFoodCount(Food.BatteredCod);
            int haddockCount = getFoodCount(Food.BatteredHaddock);
            if (codCount == FoodInOrder.EMPTY && haddockCount == FoodInOrder.EMPTY) {
                return (int) Time.ZERO;
            }

            int codCookTime = Food.BatteredCod.getCookingTime();
            int haddockCookTime = Food.BatteredHaddock.getCookingTime();
            int haddockRemainder = haddockCount % 4;

            if ( haddockCount == FoodInOrder.EMPTY ) {
                return (codCount / 4 + (codCount % 4 > FoodInOrder.EMPTY ? 1 : 0)) * codCookTime;
            } else if ( codCount == FoodInOrder.EMPTY ) {
                return (haddockCount / 4 + (haddockRemainder > FoodInOrder.EMPTY ? 1 : 0)) * haddockCookTime;
            } else {
                int diff = haddockCookTime - codCookTime;
                int fishRemainder = (haddockCount + codCount) % 4;

                return  (haddockCount/4) * Food.BatteredHaddock.getCookingTime() +
                        (haddockRemainder + codCount) /4 * Food.BatteredCod.getCookingTime() +
                        (fishRemainder > FoodInOrder.EMPTY ? Food.BatteredCod.getCookingTime() : Time.ZERO) +
                        (((fishRemainder + haddockRemainder > 4) || (fishRemainder == FoodInOrder.EMPTY)) ?
                                diff : Time.ZERO);
            }
        }

        @Override
        public boolean isFirstCookedFoodCold() {
            int firstCookedFoodCookingTime = containsFoodType(Food.BatteredCod) ?
                    Food.BatteredCod.getCookingTime() : Food.BatteredHaddock.getCookingTime();
            return getServeTime() - firstCookedFoodCookingTime > Order.MAX_COOKED_DURATION;
        }
    }

    private static final class ChipsFoodOrder extends FoodInOrder {
        public static final String TAG = ChipsFoodOrder.class.getSimpleName();

        public ChipsFoodOrder(HashMap<Food, Integer> foods) {
            super(foods);

            for (Food food: foods.keySet()) {
                if (food != Food.Chips){
                    throw new IllegalArgumentException("Not chips!");
                }
            }
        }

        @Override
        public Integer getServeTime() {
            int chipsCount = getFoodCount(Food.Chips);
            return chipsCount != FoodInOrder.EMPTY ?
                    (chipsCount / 4 + (chipsCount % 4 > FoodInOrder.EMPTY ? 1 : 0)) * Food.Chips.getCookingTime() :
                    Time.ZERO;
        }

        @Override
        public boolean isFirstCookedFoodCold() {
            return getServeTime() - Food.Chips.getCookingTime() > Order.MAX_COOKED_DURATION;
        }
    }

    public static final class Order implements Comparable<Order>, Comparator<Order> {
        public static final String TAG = Order.class.getSimpleName();

        private static final String SPACE_SEPARATOR = " ";
        private static final String COMMA_SEPARATOR = ",";
        private static final String EMPTY_STRING = "";

        private static final int MAX_COOKED_DURATION = 120; // in second
        private static final int MAX_ORDER_DURATION = 600; // in second

        private final HashMap<String, FoodInOrder> mFoodInOrders;

        private final Time mOrderTime;

        private int mOrderNumber;

        public Order(String orderInStr) {
            mFoodInOrders = new HashMap<>();
            mOrderTime = new Time();

            deserialize(orderInStr);
        }

        /**
         * deserialize string order to Order Object
         *
         * @param orderInStr "Order #1, 12:00:00, 2 Cod, 4 Haddock, 3 Chips" format
         */
        private void deserialize(String orderInStr) {
            HashMap<Food, Integer> fishOrder = new HashMap<>();
            HashMap<Food, Integer> chipsOrder = new HashMap<>();

            String[] parts = orderInStr.split(SPACE_SEPARATOR);

            mOrderNumber = Integer.valueOf(parts[1].substring(1, parts[1].length()-1));
            mOrderTime.setTime(parts[2].substring(0, parts[2].length()-1));

            int count = 3;
            while (count < parts.length) {
                Food food = Food.fromString(parts[count+1].replace(COMMA_SEPARATOR, EMPTY_STRING));
                int foodOrderCount = Integer.valueOf(parts[count]);
                if (foodOrderCount < FoodInOrder.EMPTY) {
                    throw new IllegalArgumentException("Invalid food amount");
                } else {
                    if (food == Food.Chips) {
                        chipsOrder.put(food, foodOrderCount);
                    } else {
                        fishOrder.put(food, foodOrderCount);
                    }
                    count += 2;
                }
            }

            mFoodInOrders.put(FishFoodOrder.TAG, new FishFoodOrder(fishOrder));
            mFoodInOrders.put(ChipsFoodOrder.TAG, new ChipsFoodOrder(chipsOrder));
        }

        public int getServeTimeInSecond() {
            return Collections.max(mFoodInOrders.values()).getServeTime();
        }

        public int getServeDiff(String foodOrderTag) {
            int maxServeTime = Collections.max(mFoodInOrders.values()).getServeTime();
            int foodServeTime = mFoodInOrders.get(foodOrderTag).getServeTime();
            return Math.abs(maxServeTime - foodServeTime);
        }

        public Time getOrderTime() {
            return mOrderTime;
        }

        public int getFoodCount(String foodOrderTag, Food food) {
            return mFoodInOrders.containsKey(foodOrderTag) ?
                    mFoodInOrders.get(foodOrderTag).getFoodCount(food) : FoodInOrder.EMPTY;
        }

        /**
         * Check if there is any cooked food in the order will be waited more than 120 seconds
         *
         * @return false if it does not match the quality standard
         */
        private boolean isFirstCookedFoodCold() {
            for (FoodInOrder foodInOrder: mFoodInOrders.values()) {
                if (foodInOrder.isFirstCookedFoodCold()) {
                    return true;
                }
            }
            return false;
        }

        /**
         * Check if the order meet the quality standard
         *
         * @return false if it does not match the quality standard
         */
        public boolean isOrderAccepted(Time startCookingTime) {
            return !isFirstCookedFoodCold() && !isServeTimeTooLong(startCookingTime);
        }

        /**
         * Check if the serve time is 600 seconds more than the order time
         *
         * @return false if it does not match the quality standard
         */
        private boolean isServeTimeTooLong(Time startCookingTime) {
            Time serveTime = new Time(startCookingTime);
            serveTime.add(getServeTimeInSecond());

            if (serveTime.diff(mOrderTime) < Time.ZERO) {
                throw new IllegalStateException("No pre-cook!");
            } else {
                return serveTime.diff(mOrderTime) > MAX_ORDER_DURATION;
            }
        }

        @Override
        public String toString() {
            return new StringBuilder(TAG).append(" #").append(mOrderNumber).toString();
        }

        @Override
        public int compareTo(Order anotherOrder) {
            return getOrderTime().compareTo(anotherOrder.getOrderTime());
        }

        @Override
        public int compare(Order orderOne, Order orderTwo) {
            return orderOne.getOrderTime().compareTo(orderTwo.getOrderTime());
        }
    }

    /**
     * The interface of the fryer
     */
    private interface IFryer {
        /**
         * delay the begin cook time.
         *
         * @param order the next order
         */
        void addDelay(Order order);

        /**
         * start cooking food in order
         *
         * @return the fryer cooking log
         */
        StringBuilder beginCooking();

        /**
         * get the next possible cooking time.
         *
         * @return the cooking time which is ready for the next order
         */
        Time getBeginCookingTime();

        /**
         * get the fryer tag.
         * @return fryer tag.
         */
        String getTag();

        /**
         * check if the fryer is still working for the order.
         *
         * @return true if the order is not finished, else false
         */
        boolean hasOrder();

        /**
         * receive and update the food order which the fryer need to cook.
         *
         * @param order the next order from kitchen
         */
        void receiveOrder(Order order);

        /**
         * set the initial cook time for each order.
         *
         * @param startTime the Time object of initial cook time
         */
        void setStartCookingTime(Time startTime);
    }

    private static abstract class Fryer implements IFryer, Comparable<Fryer>, Comparator<Fryer> {
        protected final int mNumber;

        public Fryer() {
            mNumber = 0;
        }

        @Override
        public int compareTo(Fryer otherFryer) {
            if (!hasOrder() || !otherFryer.hasOrder()){
                throw new IllegalStateException("No more order for the fryer!");
            } else {
                int compareValue = getBeginCookingTime().compareTo(otherFryer.getBeginCookingTime());
                if (compareValue == 0 && hashCode() != otherFryer.hashCode()){
                    return Integer.compare(hashCode(), otherFryer.hashCode());
                }
                return compareValue;
            }
        }

        @Override
        public int compare(Fryer fryerOne, Fryer fryerTwo) {
            if (!fryerOne.hasOrder() || !fryerTwo.hasOrder()){
                throw new IllegalStateException("No more order for the fryer!");
            } else {
                int compareValue = fryerOne.getBeginCookingTime().compareTo(fryerTwo.getBeginCookingTime());
                if (compareValue == 0 && fryerOne.hashCode() != fryerTwo.hashCode()){
                    return Integer.compare(fryerOne.hashCode(), fryerTwo.hashCode());
                }
                return compareValue;
            }
        }
    }

    private static final class ChipFryer extends Fryer {
        private static final String TAG = ChipFryer.class.getSimpleName();

        private static final int BATCH = 4;

        private final Time mCookTime;
        private final Time mServeTime;

        private int mChipsOrderCount;

        ChipFryer() {
            mCookTime = new Time();
            mServeTime = new Time();
        }

        @Override
        public void addDelay(Order order) {
            mServeTime.setTime(mCookTime);
            mServeTime.add(order.getServeTimeInSecond());

            mCookTime.add(order.getServeDiff(ChipsFoodOrder.TAG));
        }

        @Override
        public StringBuilder beginCooking() {
            if (!hasOrder()) {
                throw new IllegalStateException("No pending Chips order!");
            }

            StringBuilder log = LogHelper.getBeginCookingLog(
                    mCookTime, mChipsOrderCount > BATCH ? BATCH : mChipsOrderCount,
                    Food.Chips.getName()).append('\n');

            mChipsOrderCount -= BATCH;
            mCookTime.add(Food.Chips.getCookingTime());

            if (hasOrder()) {
                if (mServeTime.diff(mCookTime) > Order.MAX_COOKED_DURATION){
                    throw new IllegalStateException("First cooked food is cold!");
                }
            }

            return log;
        }

        @Override
        public Time getBeginCookingTime() {
            return mCookTime;
        }

        @Override
        public String getTag() {
            return TAG;
        }

        @Override
        public boolean hasOrder() {
            return mChipsOrderCount > FoodInOrder.EMPTY;
        }

        @Override
        public void receiveOrder(Order order) {
            mChipsOrderCount = order.getFoodCount(ChipsFoodOrder.TAG, Food.Chips);
        }

        @Override
        public void setStartCookingTime(Time startTime) {
            mCookTime.setTime(startTime);
        }
    }

    private static final class FishFryer extends Fryer{
        private static final String TAG = FishFryer.class.getSimpleName();

        private static final int MAX_COOK = 4;

        private final List<Time> mCookTimes;

        private final Time mServeTime;

        private int mCookCalibration;
        private int mCodOrderCount;
        private int mHaddockOrderCount;

        public FishFryer() {
            mCodOrderCount = FoodInOrder.EMPTY;
            mCookCalibration = FoodInOrder.EMPTY;
            mCookTimes = new ArrayList<>();
            mHaddockOrderCount = FoodInOrder.EMPTY;
            mServeTime = new Time();
        }

        @Override
        public void addDelay(Order order) {
            mServeTime.setTime(getBeginCookingTime());
            mServeTime.add(order.getServeTimeInSecond());

            for (Time time : mCookTimes){
                time.add(order.getServeDiff(FishFoodOrder.TAG));
            }
        }

        @Override
        public StringBuilder beginCooking() {
            if (!hasOrder()) {
                throw new IllegalStateException("No pending Fish order!");
            }

            StringBuilder log = new StringBuilder();

            int pos = mCookTimes.indexOf(getBeginCookingTime());
            int cookAvailable = pos == 0 ? MAX_COOK - mCookCalibration : mCookCalibration;

            if (cookAvailable <= FoodInOrder.EMPTY) {
                throw new IllegalArgumentException("Invalid cook amount");
            }

            // to serve all fish as quick as possible, cook all haddock first and then cod.
            if (mHaddockOrderCount > FoodInOrder.EMPTY) {
                Time beginCookingTime = new Time(mCookTimes.get(pos));

                int cookAmount =
                        mHaddockOrderCount > cookAvailable ? cookAvailable : mHaddockOrderCount;

                log.append(LogHelper.getBeginCookingLog(beginCookingTime,
                        cookAmount, Food.BatteredHaddock.getName())).append('\n');

                mCookTimes.get(pos).add(Food.BatteredHaddock.getCookingTime());
                mHaddockOrderCount -= cookAvailable;

                // if all haddock is cooked and the fish fryer still available to cook, start
                // cooking cod before all haddock is done cooking.
                if (mHaddockOrderCount < FoodInOrder.EMPTY && mCodOrderCount > FoodInOrder.EMPTY) {
                    mCookCalibration = Math.abs(mHaddockOrderCount);

                    // e.g. at 12:00:20, Begin Cooking 2 Cod, 2 Haddock
                    log = new StringBuilder("at ")
                            .append(beginCookingTime.toString()).append(", ")
                            .append("Begin Cooking ")
                            .append(mCodOrderCount > mCookCalibration ? mCookCalibration : mCodOrderCount)
                            .append(" ").append(Food.BatteredCod.getName()).append(", ")
                            .append(cookAmount)
                            .append(" ").append(Food.BatteredHaddock.getName()).append('\n');

                    Time newCookTime = new Time(beginCookingTime);
                    newCookTime.add(Food.BatteredCod.getCookingTime());
                    mCookTimes.add(newCookTime);
                    mCodOrderCount -= mCookCalibration;
                }

            } else {
                log.append(LogHelper.getBeginCookingLog(mCookTimes.get(pos),
                        mCodOrderCount > cookAvailable ? cookAvailable : mCodOrderCount,
                        Food.BatteredCod.getName())).append('\n');

                mCookTimes.get(pos).add(Food.BatteredCod.getCookingTime());
                mCodOrderCount -= cookAvailable;
            }

            if (hasOrder()) {
                if (mServeTime.diff(getBeginCookingTime()) > Order.MAX_COOKED_DURATION){
                    throw new IllegalStateException("First cooked food is cold!");
                }
            }

            return log;
        }

        @Override
        public Time getBeginCookingTime() {
            return Collections.min(mCookTimes);
        }

        @Override
        public String getTag() {
            return TAG;
        }

        @Override
        public boolean hasOrder() {
            return mCodOrderCount > FoodInOrder.EMPTY || mHaddockOrderCount > FoodInOrder.EMPTY;
        }

        @Override
        public void receiveOrder(Order order) {
            mCodOrderCount = order.getFoodCount(FishFoodOrder.TAG, Food.BatteredCod);
            mHaddockOrderCount = order.getFoodCount(FishFoodOrder.TAG, Food.BatteredHaddock);
        }

        @Override
        public void setStartCookingTime(Time startTime) {
            mCookCalibration = 0;

            mCookTimes.clear();
            mCookTimes.add(new Time(startTime));
        }
    }

    // main function
    public static void main(String args[] ) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        FinlayFishAndChip shop  = new FinlayFishAndChip();

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            shop.placeOrder(scanner.nextLine());
        }

        shop.executeOrders();
        System.out.println(shop.getKitchenLog());
    }
}