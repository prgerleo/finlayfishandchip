package com.leochudevelop;

/**
 * Created by leochu on 8/9/16.
 */
public class Main {
    public static void main(String[] args) {
        Solution.FinlayFishAndChip shop  = new Solution.FinlayFishAndChip();

        StringBuilder orderStringBuilder = null;
        for (String arg: args){
            if (arg.equals(Solution.Order.TAG)) {
                if (orderStringBuilder != null) {
                    String orderInString = orderStringBuilder.toString();

                    shop.placeOrder(orderInString.substring(0, orderInString.length()-1));
                }
                orderStringBuilder = new StringBuilder(Solution.Order.TAG).append(" ");
            } else {
                if (orderStringBuilder != null) {
                    orderStringBuilder.append(arg).append(" ");
                }
            }
        }
        if (orderStringBuilder != null) {
            shop.placeOrder(orderStringBuilder.toString());
        }

        shop.executeOrders();
        System.out.println(shop.getKitchenLog());
    }
}
